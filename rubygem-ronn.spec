%global gem_name ronn
Name:                rubygem-%{gem_name}
Version:             0.7.3
Release:             1
Summary:             Manual authoring tool
License:             MIT
URL:                 https://github.com/rtomayko/ronn
Source0:             http://rubygems.org/gems/ronn-%{version}.gem
BuildArch:           noarch
BuildRequires:       rubygems-devel
Requires:            rubygem(hpricot) rubygem(rdiscount) rubygem(mustache) rubygems groff-base
Requires:            ruby(release)
%description
Ronn builds manuals. It converts simple, human readable text files to
roff for terminal display, and also to HTML for the web.
The source format includes all of Markdown but has a more rigid structure and
syntax extensions for features commonly found in man pages (definition lists,
link notation, etc.). The ronn-format(7) manual page defines the format in
detail.

%package doc
Summary:             Documentation for %{name}
BuildArch:           noarch
%description doc
Documentation for %{name}

%prep
gem unpack %{SOURCE0}
%setup -q -D -T -n  %{gem_name}-%{version}
gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
mkdir -p .%{gem_dir}
gem build %{gem_name}.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a ./%{gem_dir}/* %{buildroot}%{gem_dir}/
chmod -x %{buildroot}%{gem_instdir}/lib/%{gem_name}.rb
mkdir -p %{buildroot}%{_bindir}
cp -a ./%{_bindir}/* %{buildroot}%{_bindir}
install -D -m 0644 %{buildroot}%{gem_instdir}/man/%{gem_name}.1 %{buildroot}/%{_mandir}/man1/%{gem_name}.1
install -D -m 0644 %{buildroot}%{gem_instdir}/man/%{gem_name}-format.7 %{buildroot}/%{_mandir}/man7/%{gem_name}-format.7
rm -rf %{buildroot}%{gem_instdir}/{INSTALLING,Rakefile,test,man,ronn.gemspec,config.ru}

%files
%dir %{gem_instdir}
%doc %{gem_instdir}/[A-Z]*
%{gem_instdir}/bin
%{gem_libdir}
%{gem_cache}
%{gem_spec}
%{_bindir}/%{gem_name}
%{_mandir}/man1/%{gem_name}.1*
%{_mandir}/man7/%{gem_name}-format.7*

%files doc
%{gem_docdir}

%changelog
* Fri Aug 7 2020 yanan li <liyanan032@huawei.com> - 0.7.3-1
- Package init
